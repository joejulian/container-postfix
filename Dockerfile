FROM registry.gitlab.com/joejulian/docker-arch:latest

RUN pacman -Syu --noconfirm grep postfix postfix-ldap postfix-cdb postfix-lmdb postfix-mysql postfix-pcre postfix-pgsql postfix-sqlite sed && pacman -Scc --noconfirm

CMD mkdir -p /config /socket

CMD postfix -c /config start-fg
